<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Debug\Debug;

define('REQUEST_MICROTIME', microtime(true));
define('IS_DEV', true);
define('WEBDIR', __DIR__);
define('BASE', realpath(WEBDIR . '/..'));

$loader = require_once BASE . '/app/bootstrap.php.cache';

$version = 'prod';
ini_set('display_errors', false);
if (IS_DEV) {
    $version = 'dev';
    Debug::enable(E_ALL, true);
}

require_once BASE .'/app/AppKernel.php';
require_once BASE .'/app/AppCache.php';

$kernel = new AppKernel($version, boolval($version == 'dev'));
$kernel->loadClassCache('classes', '.php.cache');
// $kernel = new AppCache($kernel);

Request::enableHttpMethodParameterOverride();
$request  = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();

$kernel->terminate($request, $response);
