<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Validator\Constraints;
use Doctrine\ORM\EntityRepository;

class RequestType extends AbstractType
{
    
    public function getName()
    {
        return 'app_form_request';
    }
    
    public function getDefaultOptions()
    {
        return array(
            'data_class' => 'AppBundle\Entity\Post'
        );
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('content', 'textarea', array(
            'required' => true,
            'label'    => 'Message',
            'constraints' => array(
                new Constraints\NotBlank(),
            )
        ));
        
        $builder->add('submit', 'submit', array(
            'label' => 'Sumbit'
        ));
    }
    
}