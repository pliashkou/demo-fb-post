<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Access
 *
 * @ORM\Table(name="facebook_access")
 * @ORM\Entity
 */
class Access extends AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="userId", type="string", length=255)
     */
    protected $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string", length=255)
     */
    protected $token;
    
    /**
     * 
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Post", mappedBy="access", cascade={"persist"})
     */
    protected $posts;
    
    public function __construct(array $params = array())
    {
        $this->posts = new \Doctrine\Common\Collections\ArrayCollection();
        parent::__construct($params);
    }
    
    /**
     *
     * @param  \AppBundle\Entity\Post $post
     * @return \AppBundle\Entity\Access
     */
    public function postAppend(Post $post)
    {
        $this->posts[] = $post;
        return $this;
    }
    
    /**
     *
     * @param  \AppBundle\Entity\Post $post
     * @return \AppBundle\Entity\Access
     */
    public function postRemove(Post $post)
    {
        $this->posts->removeElement($post);
        return $this;
    }
    
}
