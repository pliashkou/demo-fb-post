<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Post
 *
 * @ORM\Table(name="facebook_post")
 * @ORM\Entity
 */
class Post extends AbstractEntity
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="string", length=255)
     */
    protected $content;
    
    /**
     *  
     * @ORM\Column(type="datetime") 
     */
    protected $created;
    
    /**
     *  
     * @ORM\Column(type="datetime", nullable=true) 
     */
    protected $posted;
    
    /**
     * @var \AppBundle\Entity\Access
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Access", inversedBy="posts")
     */
    protected $access;
    
    public function __construct()
    {
        $this->created = new \DateTime();
    }
}
