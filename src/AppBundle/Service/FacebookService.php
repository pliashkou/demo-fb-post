<?php 

namespace AppBundle\Service;

use Symfony\Component\DependencyInjection\ContainerAware;

use AppBundle\Entity\RequestEntity;
use AppBundle\Entity;


class FacebookService extends ContainerAware
{
    
    public function __construct($appId, $secret)
    {
        if(!$appId || !$secret) {
            throw new \Exception('Not found facebook.appId or facebook.secret');
        }
        
        \Facebook\FacebookSession::setDefaultApplication($appId, $secret);
    }
    
    public function getFacebookRedirect()
    {
        return $this->container->get('router')->generate('facebook_session', array(), true);
    }
    
    public function getLoginUrl()
    {
        $redirect = $this->getFacebookRedirect();
        $helper = new \Facebook\FacebookRedirectLoginHelper($redirect);
        return $helper->getLoginUrl(array(
            'publish_actions',
        ));
    }
    
    /**
     * 
     * @return \Doctrine\ORM\EntityRepository
     */
    public function getAccess()
    {
        return $this->container->get('doctrine')->getRepository('AppBundle:Access');
    }
    
    public function updateAccess()
    {
        $session = null;
        $redirect = $this->getFacebookRedirect();
        $helper = new \Facebook\FacebookRedirectLoginHelper($redirect);
        try {
            $session = $helper->getSessionFromRedirect();
        }
        catch (\Exception $e) {
            if(IS_DEV) {
                echo join('<br />', array($e->getMessage(), $e->getTraceAsString()));
                exit(__METHOD__);
            }
            
            $this->get('logger')->erro($e->getMessage());
            $this->get('logger')->erro($e->getTraceAsString());
            return false;
        }
        
        $token = $session->getAccessToken();
        
        $response = (new \Facebook\FacebookRequest($session, 'GET', '/me'))->execute();
        $userId = $response->getGraphObject()->getProperty('id');
        
        $entity = $this->getAccess()->findOneBy(array('userId' => $userId));
        if (!$entity) {
            $entity = new Entity\Access(array('userId'=>$userId, 'token'=>$token));
        }
        else if ($entity) {
            $entity->setToken($token);
        }
        
        if($entity instanceof Entity\Access) {
            $em = $this->container->get('doctrine')->getManager();
            $em->persist($entity);
            $em->flush();
            $em->clear();
        }
        
        $this->container->get("session")->set("facebook_token", $token);
        $this->container->get("session")->set("facebook_userId", $userId);
        
        return true;
    }
    
    /**
     *
     * @return \Doctrine\ORM\EntityRepository
     */
    public function getPost()
    {
        return $this->container->get('doctrine')->getRepository('AppBundle:Post');
    }
    
    /**
     * @return string
     */
    public function getPostPagination($userId, $page = 1, $count = 10)
    {
        $em = $this->container->get('doctrine')->getManager();
        $em instanceof \Doctrine\ORM\EntityManager;
        $query = $em->createQueryBuilder()
            ->select('p')
            ->from('AppBundle:Post', 'p')
            ->join('p.access', 'a')
            ->where('a.userId = :userId')
            ->orderBy('p.id', 'DESC')
            ->setParameter('userId', $userId);
        return $this->container->get('knp_paginator')->paginate($query, $page, $count);
    }
    
    public function addPost($userId, Entity\Post $entity)
    {
        $access = $this->getAccess()->findOneBy(array('userId' => $userId));
        $entity->setAccess($access);
        
        $em = $this->container->get('doctrine')->getManager();
        $em->persist($entity);
        $em->flush();
        
        if($entity->getId()) {
            return true;
        }
        
        return false;
    }
    
    public function setPosted(Entity\Post $entity)
    {
        $entity->setPosted(new \DateTime());
        
        $em = $this->container->get('doctrine')->getManager();
        $em->persist($entity);
        $em->flush();
        
        return $this;
    }
}